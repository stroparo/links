# Links

Not maintained here anymore, please see these:

* https://github.com/stroparo/links#links
* https://bitbucket.org/stroparo/links/src/master/README.md

Or:

[Software development links](https://github.com/stroparo/devlinks#dev-links)
